class ItemsController < ApplicationController
  before_action :authenticate_user!
  
  def new
    sale_order = SaleOrder.get_active(current_user)
    product = Product.find(params[:product_id])
    @item = Item.new({product: product, sale_order: sale_order})
  end

  def index
  end

  def show
  end
  
  def create
    @item = Item.new(item_params) 
    @item.save
     
    redirect_to products_path
  end
  
  def destroy
    item = Item.find(params[:id])
    item.destroy
    redirect_to products_path
  end
  
  private 
      def item_params
        params.require(:item).permit(:quantity, :product_id, :sale_order_id)
      end
end
