class SalesController < ApplicationController
  before_action :authenticate_user!
  
  def new
    @sale = Sale.new
    @sale.sale_order_id = SaleOrder.get_active(current_user).id
    @customer_id        = $customer["id"]
    @customer_cards     = $mp.get("/v1/customers/#{@customer_id}/cards")["response"]
    @payment_ways       = $mp.get("/v1/payment_methods") 
  end
  
  def create; do_pay; end
  def update; do_pay; end
  
  def do_pay
    
    @payment = Hash.new
    
    @customer_id    = $customer["id"]
    @customer_cards = $mp.get("/v1/customers/#{@customer_id}/cards")["response"]
    @payment_ways   = $mp.get ("/v1/payment_methods")
    
    @redirect = nil
    
    @sale = Sale.create({
      token: params[:token],
      payment_method_id: params[:paymentMethodId],
      sale_order_id: SaleOrder.get_active(current_user).id
    })
    
    kind_of_operation = params[:kindOfOperation]
    
    payment_data = {
      transaction_amount: @sale.sale_order.total,
      token:              params[:token],
      description:        "Sample sale",
      installments:       params[:installments].to_i || 1,
      issuer_id:          params[:issuer],
      payment_method_id:  params[:paymentMethodId] || params[:kindPayment],
      capture:            true,
      payer: {
        email: params[:email]
      }
    }
    
    @payment = $mp.post("/v1/payments", payment_data);
    
    case kind_of_operation
      when "card"  
        $mp.post("/v1/customers/#{$customer['id']}/cards", {token: params[:token]}) if params[:saveCard] == "1"
      when "wallet"  
        # Nothing
      when "other"  
        @redirect = @payment["response"]["transaction_details"]["external_resource_url"] rescue nil
    end
  
    case @payment["status"]
      when "400"
        @error = @payment["response"]["message"]
        render :new
      when "201"
        @sale.sale_order.finish!
        render :create 
    end
  end

end
