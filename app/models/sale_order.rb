class SaleOrder < ActiveRecord::Base
  
  has_one :sale
  has_many :items
  
  state_machine :state, :initial => :created do
    
    event :finish do
      transition :created => :finished
    end
    
    event :cancel do
      transition :created => :canceled
    end
    
  end
  
  def self.get_active(user)
    sale_order = SaleOrder.where("state = 'created' AND user_id = ?", user["id"]).last || SaleOrder.create({state: "created", user_id: user["id"]})
  end
  
  def total
    self.items.map{|i| i.product.price * i.quantity}.reduce(:+)
  end
  
  
  
end
